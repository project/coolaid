function getViewportHeight() {
	var viewportheight;
	// the more standards compliant browsers (mozilla/netscape/opera/IE7) use window.innerWidth and window.innerHeight
	if (typeof window.innerWidth != 'undefined') {
	  viewportheight = window.innerHeight
	}
	else if (typeof document.documentElement != 'undefined'
	 && typeof document.documentElement.clientWidth !=
	 'undefined' && document.documentElement.clientWidth != 0) {
	   viewportheight = document.documentElement.clientHeight
	}
	// older versions of IE
	else {
	   viewportheight = document.getElementsByTagName('body')[0].clientHeight
	}
    return viewportheight;
}

$(document).ready(function() {

	// setup	
	$("div.coolaid").before("<a title='Help' class='coolaid-launch' href='#'>(?)</a>");
	var coolaid_width = $("div.coolaid").width();
	var coolaid_height = $("div.coolaid").height();
	$("div.coolaid").hide();
	$("body").append("<div class='coolaid-sup'>"+$("div.coolaid").html()+"</div>");
	$("div.coolaid-sup div.coolaid-content").prepend("<a title='Close' class='coolaid-close' href='#'>(X)</a>");
	$("div.coolaid-sup").hide();
	$("body").append("<div id='coolaid-overlay'></div>");
    $("div#coolaid-overlay").fadeTo(0, 0);
	$("div#coolaid-overlay").hide();

	// show overlay
	$("a.coolaid-launch").click(function() {
		viewportheight = getViewportHeight();
		var max_height = viewportheight*0.8;
		if (coolaid_height < max_height) {
			max_height = coolaid_height;
		}
		var top_offset = (viewportheight-max_height)/2;
		$("div.coolaid-sup").css('top', top_offset+'px');
		$("div.coolaid-sup").css('height', max_height+'px');
		$("div.coolaid-sup").css('position', 'absolute');
		$("div.coolaid-sup").css('width', coolaid_width+'px');
		$("div.coolaid-sup").css('left', '50%');
		$("div.coolaid-sup").css('margin-left', -(coolaid_width/2));
		$("div#coolaid-overlay").css('width', $("html").width());
		$("div#coolaid-overlay").css('height', $("html").height());
		$("div#coolaid-overlay").show();
		$("div#coolaid-overlay").fadeTo("fast", 0.66);
		$("div.coolaid-sup").show();
	});

	// hide overlay
	$("div#coolaid-overlay, a.coolaid-close").click(function() {
		$("div.coolaid-sup").hide();
		$("div#coolaid-overlay").fadeTo("slow", 0);
		$("div#coolaid-overlay").hide();
	});

});
